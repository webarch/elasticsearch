# Ansible Elasticsearch

An Ansible role to [install Elasticsearch on Debian](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html).
